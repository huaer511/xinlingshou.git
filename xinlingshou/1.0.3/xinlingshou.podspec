
Pod::Spec.new do |s|

  s.name         = "xinlingshou"
  s.version      = "1.0.3"
  s.summary      = "xinlingshou by ios " 
  s.description  = "xinlingshou"
  s.homepage     = "https://gitee.com/huaer511/xinlingshou.git"
  s.license      = "MIT"
  # s.license    = { :type => "MIT", :file => "FILE_LICENSE" }
  s.author       = { "周文飞" => "zhouwf511@126.com" }
  s.platform     = :ios, "8.0"
  s.source       = { :git => "https://gitee.com/huaer511/xinlingshou.git", :tag => "#{s.version}" }
  s.source_files = "DemoView/*.{h,m}" 
  s.resource     = "Resources/*.xib"
  s.requires_arc =  true
  s.frameworks = 'UIKit'
  s.dependency 'OpenUDID', '~> 1.0.0'

end
