//
//  JXTableViewCell.m
//  testDemo
//
//  Created by Zhou Wenfei on 2018/7/31.
//  Copyright © 2018年 kuangshi. All rights reserved.
//

#import "JXTableViewCell.h"

@implementation JXTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
